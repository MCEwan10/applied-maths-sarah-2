﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;

namespace Hit_Box_Example
{
    class GameObject
    {
        private Model model;
        private Matrix[] transforms;

        public Vector3 position;
        public Vector3 scale = Vector3.One;
        public Vector3 rotation;
        public Vector3 collisionScale = Vector3.One;
        public Vector3 collisionOffset = Vector3.Zero;
        public bool colliding = false;

        // Behaviour

        public void LoadModel(ContentManager content, string name)
        {
            model = content.Load<Model>(name);

            transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);
        }

        public void Draw()
        {
            //create matrices to help drawing
            
            // ------------------------------
            // VIEW MATRIX
            // ------------------------------
            // This puts the model in relation to where our camera is, and the direction of our camera.
            Matrix view = Matrix.CreateLookAt(
                new Vector3(0, 400, -1000),
                Vector3.Zero, // target
                Vector3.Up
                );

            // ------------------------------
            // PROJECTION MATRIX
            // ------------------------------
            // Projection changes from view space (3D) to screen space (2D)
            // Can be either orthographic or perspective

            // Perspective
            Matrix projection = Matrix.CreatePerspectiveFieldOfView(
                MathHelper.ToRadians(45),
                16f / 9f,
                1f,
                100000f);

            // Loop through the meshes in the 3D model, drawing each one in turn
            foreach (ModelMesh mesh in model.Meshes)
            {
                // Loop through each effect on the mesh
                foreach (BasicEffect effect in mesh.Effects)
                {
                    // To render we need three things - world matrix, view matrix, and projection matrix
                    // But we actually start in model space - this is where our world starts before transforms

                    // ------------------------------
                    // MESH BASE MATRIX
                    // ------------------------------
                    // Our meshes start with world = model space, so we use our transforms array
                    effect.World = transforms[mesh.ParentBone.Index];


                    // ------------------------------
                    // WORLD MATRIX
                    // ------------------------------
                    // Transform from model space to world space in order - scale, rotation, translation.

                    // Scale
                    // Scale our model by multiplying the world matrix by a scale matrix
                    // XNA does this for use using CreateScale()
                    effect.World *= Matrix.CreateScale(scale);

                    // Rotation
                    // Rotate our model in the game world
                    effect.World *= Matrix.CreateRotationX(rotation.X); // Rotate around the x axis
                    effect.World *= Matrix.CreateRotationY(rotation.Y); // Rotate around the y axis
                    effect.World *= Matrix.CreateRotationZ(rotation.Z); // Rotate around the z axis

                    // Translation / position
                    // Move our model to the correct place in the game world
                    effect.World *= Matrix.CreateTranslation(position);

                    // ------------------------------
                    // VIEW MATRIX
                    // ------------------------------
                    // This puts the model in relation to where our camera is, and the direction of our camera.
                    effect.View = view;

                    // ------------------------------
                    // PROJECTION MATRIX
                    // ------------------------------
                    // Projection changes from view space (3D) to screen space (2D)
                    // Can be either orthographic or perspective

                    // Perspective
                    effect.Projection = projection;

                    // Orthographic
                    //effect.Projection = Matrix.CreateOrthographic(
                    //    1600, 900, 1f, 10000f
                    //    );

                    // ------------------------------
                    // LIGHTING
                    // ------------------------------
                    // Some basic lighting, feel free to tweak and experiment
                    effect.LightingEnabled = true;
                    effect.Alpha = 1.0f;
                    effect.AmbientLightColor = new Vector3(1.0f);
                }

                // Draw the current mesh using the effects we set up
                mesh.Draw();
            }
            //draw the bounds 
            Color boundsColor = Color.Gray;

            if (colliding) boundsColor = Color.White;

            //render our bounds
            BoundingRenderer.RenderBox(GetFixedAABB(), view, projection, boundsColor);


        }


        public BoundingSphere GetBoundingSphere()
        {
            BoundingSphere bounds = new BoundingSphere();
            //Use the position of our object + the center of the mesh
            bounds.Center = model.Meshes[0].BoundingSphere.Center + position + collisionOffset;

            //scale our radius based on the model, our gameObject scale and collision scale
            //just use the X scale, as collision spheres are the same in all directions
            bounds.Radius = model.Meshes[0].BoundingSphere.Radius * scale.X * collisionScale.X;
            return bounds;
        }

        public BoundingBox GetFixedAABB()
        {
            BoundingBox bounds = new BoundingBox();

            bounds.Min -= collisionScale * scale * 0.5f - new Vector3(model.Meshes[0].BoundingSphere.Radius);
            bounds.Max += collisionScale * scale * 0.5f + new Vector3(model.Meshes[0].BoundingSphere.Radius);

            bounds.Min += position + collisionOffset + model.Meshes[0].BoundingSphere.Center;
            bounds.Max += position + collisionOffset + model.Meshes[0].BoundingSphere.Center;
            return bounds;
        }

        public BoundingBox GetDynamicAABB()
        {
            BoundingBox bounds = new BoundingBox();

            foreach(ModelMesh mesh in model.Meshes)
            {
                foreach(ModelMeshPart meshPart in mesh.MeshParts)
                {
                    //create an array to store the vertex data
                    VertexPositionNormalTexture[] modelVertices = new VertexPositionNormalTexture[meshPart.VertexBuffer.VertexCount];
                    //get model's vertices
                    meshPart.VertexBuffer.GetData(modelVertices);

                    //create new array to store each pos of each vertex
                    Vector3[] vertices = new Vector3[modelVertices.Length];

                    //get the bone transforms and then apply the trs transforms to them
                    Matrix meshTransform = mesh.ParentBone.Transform;
                    meshTransform *= Matrix.CreateScale(scale);
                    meshTransform *= Matrix.CreateScale(collisionScale);
                    meshTransform *= Matrix.CreateRotationX(rotation.X);
                    meshTransform *= Matrix.CreateRotationY(rotation.Y);
                    meshTransform *= Matrix.CreateRotationZ(rotation.Z);

                    //loop through the verticies
                    for (int i = 0; i <vertices.Length; i++)
                    {
                        vertices[i] = Vector3.Transform(modelVertices[i].Position, meshTransform);
                    }
                    bounds = BoundingBox.CreateMerged(bounds, BoundingBox.CreateFromPoints(vertices));
                }
            }

            //move box to correct place in the world
            bounds.Min += position + collisionOffset + model.Meshes[0].BoundingSphere.Center;
            bounds.Max += position + collisionOffset + model.Meshes[0].BoundingSphere.Center;

            return bounds;
        }
    }
}
