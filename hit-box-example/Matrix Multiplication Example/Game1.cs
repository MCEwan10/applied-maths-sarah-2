﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;

namespace Hit_Box_Example
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        GameObject pig = new GameObject();
        GameObject truffle = new GameObject();

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            pig.LoadModel(Content, "PigN");
            pig.collisionScale = new Vector3(200);
            pig.collisionOffset = new Vector3(0, 50, 0);

            truffle.LoadModel(Content, "TruffleN");
            truffle.collisionScale = new Vector3(100);
            truffle.collisionOffset = new Vector3(0, 20, 0);
            truffle.position = new Vector3(300, 0, 0);

            // Debug setup
            BoundingRenderer.InitializeGraphics(graphics.GraphicsDevice);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Rotate left / right
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
                pig.rotation.Y += 0.1f;
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
                pig.rotation.Y -= 0.1f;

            // Move forward
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                pig.position.X += (float)Math.Sin(pig.rotation.Y) * 1000f * deltaTime;
                pig.position.Z += (float)Math.Cos(pig.rotation.Y) * 1000f * deltaTime;
            }

            //check for collision
            bool colliding = pig.GetFixedAABB().Intersects(truffle.GetFixedAABB());
            pig.colliding = colliding;
            truffle.colliding = colliding;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            pig.Draw();
            truffle.Draw();

            base.Draw(gameTime);
        }
    }
}
